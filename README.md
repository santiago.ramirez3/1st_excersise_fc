# 1st_excersise_FC


## Interaction of two charged particles submerged in a magenetic field.

## Authors 
- [Santiago Ramirez Gaviria.](https://gitlab.com/santiago.ramirez3)
  
santiago.ramirez3@udea.edu.co
- [Sebastian Carrillo Mejia.](https://gitlab.com/sebastian.carrillo1)
  
sebastian.carrillo1@udea.edu.co



## Description
This project calculates the trajectory, velocity and acceleration of two charged particles immersed in a magnetic field using an iterative method.

Yo apply that method, it is considered the lorentz force, where the total force applied over a particle with charge Q is given by:

$$
\vec{F_Q} =  Q (\vec{E_q} + \vec{v_Q} \times \vec{B} ) 
$$

Where, using coulomb law:
$$
\vec{f_e} = \frac{Qq}{4 \pi \epsilon_0} \frac{\vec{r} - \vec{r}\prime}{|\vec{r} - \vec{r}\prime|³} = Q\vec{E_q}
$$

Therefore, we have a expressión for the acceleration on the particle $Q$, due to the presence of other particle $q$. So the differencial equation can be numerically solved, and we did that with the scripts in folder 'Method_1', where we use de module scipy.integrate.solve_ivp(), and it got nice results for the solution of several trajectories with different initial conditions.

Also, using this information, we can consider the aproximation of finite sum, which consider small time intervals for which the acceleration is cosntant along the particle movement, therefore, for each small time interval the movement of the particle can be calculated with the follwing expressions (for each particle):

$$
r = r_0 + v_0 t + \frac{1}{2} at²\\[1em]
v = v_0 + at \\[1em]
a =  \frac{Q}{m} (E_q + v_Q \times B ) \\[1em]

$$

 > [!NOTE]
 > All the expresions above, are considered to be vectors, which in python language, are said to be np.ndarrays with 3 entries, one dimension each, which means, the vector arrows are oviated.

The genereral procedure of the code for Method_2, consists in 3 archives, here you will find a brief description of the contents on each archive and their respective usage, for further information, please contact us via e-mail.

- classes.py:
 
  Here you can find the class "particles" on which the object particle is created, with its respective mass, charge, inital position and velocity.
  You will also find a method that is thougth to solve the interaction of two particles which correspond of two objects of the class; this method takes the other object of the class, a magnetic field and a time interval.

  This method, takes the initial position an velocity and with them, calculates the initial acceleration, for the next time interval, this process is repeated until the complete time interval has passed

- axuliarfunc.py:

    This module is only used to connect __main __.py and classes.py documents. this module is called by the main archive and he uses classes.py to create the particles and uses the method to solve the interaction.

- __main __.py:

    This archive is the one to run, he calls the auxiliarFunc.py giving him the needed information to create the particles and solve the interaction, from it, you can either choose 1 or 2 proposed initial conditions.

    At last the user will be asked if he wants to either save plots and data in csv files, or only the csv files.
> [!WARNING]
> This method can only be used in a not very long course of time, the used aproximation of constant acceleration in small intervals can lead to some cumulative errors that in time can be notorious and lead to non physical solutions, that problem can be partially solved using smaller time intervals, however that can lead to time convergency problems, for more information in numerical analysis, read Análisis numérico L. Burden and look for the finite sum aproximation. And because of the same aproximation it can lead some significative error when the particles are too close and the acceleration is to high, because particles can go ahead during the interval time between steps.

>The error in this aproximation can be seen by analysing the results usign the first inital condition, which start supposing that the particles are suffitiently away from each other in such a way that their electric interaction is no too strong, the result, as can be seen from Method_1 solution is expected to show helical movement, however, with this aproximation we can see that one partcle is increasing the helical radius, whcih corresponds to a non-physical solution.

## Run Locally 
First, create a python virtual enviroment and activate it.

```bash
virtualenv venv
source venv/bin/activate
```


get inside the virtual enviroment, add a new empty repo and inside it, clone using:

```bash
  git clone https://https://gitlab.com/sebastian.carrillo1/1st_excersise_fc
```

Go to the project directory (if you're not inside already) and install dependencies from requirements.txt

```bash
  cd 1st_excersise_fc
  pip install -r requirements.txt
```

## Usage
To run this code, all you need is move to Method_2 directory and execute the __ main__.py file, after that, you will be asked to choose one from two predetermined initial conditions.

## Support
If having any trouble or bugg while runnig the code, please don't hesitate and send us an e-mail, also, new recomendations for improvements are apprecaited.

Contributors e-mail can be found in the Authors section.



