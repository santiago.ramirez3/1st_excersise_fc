import classes
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d


def creatingParticles(condition:int) -> tuple:
    '''
    This function creates two objects of the class particles, given:
    - Charge: float
    - Mass: float
    - initial position: np.ndarray with 3 components
    - initial velocity: np.ndarray with 3 components
    - initial aceleration: np.ndarray with 3 components
    
    By defect values:
     - The two posible initial conditions:
     1. q1: m1 = 1, q1 = 10, r10 = np.array([1,0,0]), v10 = np.array([-7,-1,1], B = [0,0,2], t0 = 0 , tf = 2, steps = 1000)
        q2: m2 = 1, q2= 5, r20 = np.array([-2,0,0]), v20 = np.array([5,-0.7,2]) 
        
     2. q1: m = 1, q1 = 10, r10 = np.array([4,2,1]), v10 = np.array([3,0,0], B = [0,0,2.5], t0 = 0 , tf = 2, steps = 1000)
        q2: m2 = 1, q2= 5, r20 = np.array([7,2,1]), v20 = np.array([-4,0,0])
    '''
    
    if condition == 1:
        particle_1 = classes.particles(1, 10, np.array([1,0,0]), np.array([-7,-1,1]))
        particle_2 = classes.particles(1, 5, np.array([-2,0,0]), np.array([5,-0.7,2]))
    elif condition == 2:
        particle_1 = classes.particles(1, 10, np.array([0.5,0,0]), np.array([-4.,-0.5,1.]))
        particle_2 = classes.particles(1, -5, np.array([-0.5,0,0]), np.array([5.,-0.7,1.]))
    
    return particle_1, particle_2

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------------------------------------------------------------------------------------------------------------
def executingInteraction(particle_1:object, particle_2:object ,B:list,t0:float, tf:float, length:int, plot:int, name:str) -> None :
    '''
    This function uses the method of particle called interaction, in which the following values are calculated:
    - Position of both particles: r1 and r1
    - velocity of both particles: v1 and v2
    - aceleration of both particles: a1 and a2
    
    params:
    - particle_1 and particle_2: two objects from the class particles
    - t0, tf, length: initial time, final time and the amount of steps in the time interval.
    -plot: integer that can be either 1 or 0:
    1 for plotting the images and saving the data
    2 for only saving data.
    - B: magnetic fiels, 3 entry array.
    - name: string used to set the name of results.
    
    plot:
    The user can decide either plotting or not. the plots are 3D for the position of the particle
    the aceleration and velocities are vs time in a 2D plots.
    plots are stores in PLOTS directory.
    
    csv data:
    The data for the result is stored in csv file, iside the directory DATOS
    
    '''
    
    t_interval = np.linspace(t0,tf,length)

    #
    result = particle_1.interaction(particle_2,np.array(B),t_interval )
    
    #plotting data
    if plot == 1:
        print('Generating plot and saving data...')
        
    #Potting 3D position.
        
        fig = plt.figure(figsize=(8,8))
        ax = plt.axes(projection='3d')
        ax.set_title('Gráfica de trayectoria', size=18)
        ax.plot3D(result[0][:,0], result[0][:,1], result[0][:,2], label='Partícula 1',color='gold')
        ax.plot3D(result[1][:,0], result[1][:,1], result[1][:,2], label='Partícula 2',color='indigo')
        ax.set_xlabel('x (a.u)')
        ax.set_ylabel('y (a.u)')
        ax.set_zlabel('z (a.u)')
        ax.legend()
        plt.savefig('PLOTS/3Dtrajectory' + name + '.png' , dpi=250)
        
    # plotting position vs time.
        coords = [r'x $(a.u)$',r'y $(a.u)$',r'z $(a.u)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('Gráfica de posición contra tiempo', size=19)
        for i in range(3):
            ax[i].plot(t_interval ,result[0][:,i],label='Partícula 1',color='gold')
            ax[i].plot(t_interval ,result[1][:,i],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (a.u)
        
        Condiciones iniciales: $r_1=${particle_1.r[0]}, $r_2=${particle_2.r[0]}, $v_1=${particle_1.v[0]}, $v_2=${particle_2.v[0]}, $B=${B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/position(t)' + name + '.png' , dpi=250)
    
    #plotting velocity vs time
        coords = [r'vx $(a.u)$',r'vy $(a.u)$',r'vz $(a.u)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('Gráfica de velocidad contra tiempo', size=19)
        for i in range(3):
            ax[i].plot(t_interval,result[2][:,i],label='Partícula 1',color='gold')
            ax[i].plot(t_interval,result[3][:,i],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (a.u)
        
        Condiciones iniciales: $r_1=${particle_1.r[0]}, $r_2=${particle_2.r[0]}, $v_1=${particle_1.v[0]}, $v_2=${particle_2.v[0]}, $B=${B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/velocity(t)' + name + '.png' , dpi=250)
        
     #plotting acceeleration vs time.
        coords = [r'ax $(a.u)$',r'ay $(a.u)$',r'az $(a.u)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('Gráfica de aceleración contra tiempo', size=19)
        for i in range(3):
            ax[i].plot(t_interval,result[4][:,i],label='Partícula 1',color='gold')
            ax[i].plot(t_interval,result[5][:,i],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (a.u)
        
        Condiciones iniciales: $r_1=${particle_1.r[0]}, $r_2=${particle_2.r[0]}, $v_1=${particle_1.v[0]}, $v_2=${particle_2.v[0]}, $B=${B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/acceleration(t)' + name + '.png' , dpi=250)   
        
        
    elif plot == 2:
        print('Only data will be returned')   
        
        
    #Saving data in csv files
        
    positionData = pd.DataFrame( 
                                {'r1x a.u':result[0][:,0],'r1y a.u':result[0][:,1],'r1z a.u':result[0][:,2],
                                 'r2x a.u':result[1][:,0],'r2y a.u':result[1][:,1],'r2z a.u':result[1][:,2],'time a.u':t_interval} 
                                ).reset_index()
    velocityData = pd.DataFrame( 
                                {'v1x a.u':result[2][:,0],'v1y a.u':result[2][:,1],'v1z a.u':result[2][:,2],
                                 'v2x a.u':result[3][:,0],'v2y a.u':result[3][:,1],'v2z a.u':result[3][:,2],'time a.u':t_interval} 
                                ).reset_index()
    acelerationData = pd.DataFrame( 
                                {'a1x a.u':result[4][:,0],'a1y a.u':result[4][:,1],'a1z a.u':result[4][:,2],
                                 'a2x a.u':result[5][:,0],'a2y a.u':result[5][:,1],'a2z a.u':result[5][:,2],'time a.u':t_interval} 
                                ).reset_index()
    
    positionData.to_csv('DATA/position' + name + '.csv' , sep =',', index= False, encoding='utf-8')
    velocityData.to_csv('DATA/velocity' + name + '.csv' , sep =',', index= False, encoding='utf-8')
    acelerationData.to_csv('DATA/aceleration' + name + '.csv' , sep = ',', index = False, encoding='utf-8')


#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------------------------------------------------------------------------------------------------------------
