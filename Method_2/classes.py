import numpy as np

class particles:

    r:np.ndarray = np.zeros((1,3))
    v:np.ndarray = np.zeros((1,3))
    a:np.ndarray = np.zeros((1,3))

    def __init__ (self, mass:float, charge:float, r_initial:np.ndarray, v_initial:np.ndarray) -> None:
        '''
        Here the basic propertis of particles are assigned
        
        Params:
        - q: Charge of the particle
        -m: mass of the particle
        - r0 = Initial position of the particle, is a list of 3 elements (on vectorial notation)
        - v0 = initial velocity of the particle, is a list of 3 elements (on vectorial notation)
        - a0 = initial acceleration of the particle, is a list of 3 elements (on vectorial notation)
        '''
        self.q = charge
        self.m = mass
        self.r0 = r_initial
        self.v0 = v_initial
        
    def interaction (self, q2:object, B:np.ndarray, t_interval:np.ndarray) -> tuple:
        '''
        We take the particle submerged in 2 different fields, a magnetic fiel B
        and electric field from the other particle...
        - Initial_cond: initial conditions of the particles, tuple
        '''
        #--------------------------------------------------------------------------
        #------- Constants --------------------------------------------------------

        e0 = 1.
        k = 1 / (4*np.pi*e0)

        n = len(t_interval)
        
        #------- paticle_1's attributes -------------------------------------------

        m1:float = self.m
        Q1:float = self.q

        self.r = np.zeros((n,3), dtype=float)
        self.v = np.zeros((n,3), dtype=float)
        self.a = np.zeros((n,3), dtype=float)

        #------- paticle_2's attributes -------------------------------------------

        m2:float = q2.m
        Q2:float = q2.q

        q2.r = np.zeros((n,3), dtype=float)
        q2.v = np.zeros((n,3), dtype=float)
        q2.a = np.zeros((n,3), dtype=float)

        #--------------------------------------------------------------------------
        #------- Establecer condiciones iniciales ---------------------------------
        self.r[0,:] =  self.r0
        q2.r  [0,:] =    q2.r0
        
        self.v[0,:] =  self.v0
        q2.v  [0,:] =    q2.v0
        
        dr0 = self.r[0,:]-q2.r[0,:]
        iR3_0 = (1/np.dot(dr0,dr0))**1.5
        
        self.a[0,:] = Q1 * (np.cross(self.v[0,:],B) + Q2*k*iR3_0*(dr0)) / m1
        q2.a  [0,:] = Q2 * (np.cross(  q2.v[0,:],B) - Q1*k*iR3_0*(dr0)) / m2
        #--------------------------------------------------------------------------
        #------- Cycle to solve the trajectory with finite differences ------------

        for i in range(0,n-1):
            #------- Position and velocity with aproximation a=cte ----------------
            
            dt = t_interval[i+1]-t_interval[i]

            self.r[i+1,:] = self.r[i,:] + self.v[i,:]*dt + 0.5*self.a[i,:]*dt*dt
            q2.r  [i+1,:] =   q2.r[i,:] +   q2.v[i,:]*dt + 0.5*  q2.a[i,:]*dt*dt
            
            self.v[i+1,:] = self.v[i,:] + self.a[i,:]*dt
            q2.v  [i+1,:] =   q2.v[i,:] +   q2.a[i,:]*dt
            
            #------- Variables para calcular aceleracion --------------------------

            dr = self.r[i+1,:]-q2.r[i+1,:]  # Vector of particles' separation
            iR3 = (1/np.dot(dr,dr))**1.5    # Invers of cubic distance

            #------- aceleracion de cada particula --------------------------------

            self.a[i+1,:] = Q1 * (np.cross(self.v[i+1,:],B) + Q2*k*iR3*(dr)) / m1
            q2.a  [i+1,:] = Q2 * (np.cross(  q2.v[i+1,:],B) - Q1*k*iR3*(dr)) / m2
        
        return self.r, q2.r, self.v, q2.v, self.a, q2.a
    #------------------------------------------------------------------------------