#import classes
from auxiliarFunc import creatingParticles, executingInteraction


if __name__ == '__main__':
    try:
        condition:int = int(input('''
                                  Ingrese 1 o 2 según desee: \n
                                  1. q1: m1 = 1, q1 = 10, r10 = np.array([1,0,0]), v10 = np.array([-7,-1,1], B = [0,0,2], t0 = 0 , tf = 2, steps = 1000)\n
                                     q2: m2 = 1, q2= 5, r20 = np.array([-2,0,0]), v20 = np.array([5,-0.7,2]) \n
                                  2. q1: m = 1, q1 = 10, r10 = np.array([0.5,0,0]), v10 = np.array([-4.,-0.5,1.], B = [0,0,2.5], t0 = 0 , tf = 2, steps = 1000)\n
                                    q2: m2 = 1, q2= -5, r20 = np.array([-0.5,0,0]), v20 = np.array([5.,-0.7,1.])\n
                                  R/: '''))
        plot:int = int(input('''
                             Ingrese 1 o 0 según desee: \n
                             1. plot results?\n
                             2. Do not plot.\n
                             R/: '''))
        
        if (condition == 1) & (plot == 1) :
            objects:tuple = creatingParticles(condition)
            executingInteraction(objects[0],objects[1],[0,0,2],0,2,1000, plot, '_1')
            
        elif (condition == 1) & (plot == 2):
            objects:tuple = creatingParticles(condition)
            executingInteraction(objects[0],objects[1],[0,0,2],0,2,1000, plot, '_1')
            
        elif (condition == 2) & (plot == 1) :
            objects:tuple = creatingParticles(condition)
            executingInteraction(objects[0],objects[1],[0,0,2.5],0,2,1000, plot, '_2')
            
        elif (condition == 2) & (plot == 2):
            objects:tuple = creatingParticles(condition)
            executingInteraction(objects[0],objects[1],[0,0,2.5],0,2,1000, plot, '_2') 
        else:
            raise ValueError 
    except ValueError as e :
        print("Invalid value encountered, aborting... please try again.")
        
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#------------------------------------------------------------------------------------------------------------








