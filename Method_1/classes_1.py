from scipy.integrate import solve_ivp  # Module to solve de differential equations
import matplotlib.pyplot as plt        # Module to plot
import numpy as np                     # Module to use numerical objects

class particle:
    def __init__(self, mass, charge, position, velocity):
        '''
        Here are assigned the initial conditions of the particle
        '''
        self.m = mass                # Mass of the particle
        self.q = charge              # Charge of the particle
        self.r = np.array(position)  # Position of the particle
        self.v = np.array(velocity)  # Velocity of the particle


class interaction:
    '''
    This class generate objects that contains the solution of
    the movement of two interactive particles into a constant
    magnetic field given.

    To do that are defined the method .solution() that solve the 
    differential equations, and four methods to plot the results.
    '''
    def __init__(self, Particle_1, Particle_2, Mag_field, init_time, final_time, precision):
        '''
        Here are initialized the the varibles and parameters to solve the equations
        '''
        self.e0 = 1.
        self.eps = precision
        self.P1 = Particle_1
        self.P2 = Particle_2
        self.B = np.array(Mag_field)
        self.t0 = init_time
        self.tf = final_time
        self.time = np.arange(self.t0,self.tf,self.eps)
        self.ini_conditions = np.concatenate((Particle_1.r,Particle_2.r,Particle_1.v,Particle_2.v))
        #---------------------------------------------
        self.r1 = np.zeros((3,len(self.time)))
        self.r2 = np.zeros((3,len(self.time)))
        self.v1 = np.zeros((3,len(self.time)))
        self.v2 = np.zeros((3,len(self.time)))
        self.a1 = np.zeros((3,len(self.time)))
        self.a2 = np.zeros((3,len(self.time)))
        
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Metodo para solucionar el sistema de ecuaciones
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def solution(self):
        '''
        This method compute the solution of the differential equations
        using the module scipy.integrate.solve_ivp. And keep the solution
        in the attributes of the object.
        '''
        #------- Constants of the system ---------------------------------------------------------------
        
        _B=self.B
        q1, m1 = self.P1.q ,self.P1.m
        q2, m2 = self.P2.q ,self.P2.m
        k = 1/(4*np.pi*self.e0)
        
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        def ODE(t,current_conditions):   #Sistema de ecuaciones diferenciales que modela el sistema
            #------- Variables que cambian cada instante -----------------------------------------------
            
            cc = current_conditions
            _r1,_r2 ,_v1,_v2 = cc[0:3],cc[3:6],cc[6:9],cc[9:12]
            dr = _r1-_r2
            iR3 = (1/np.dot(dr,dr))**1.5
            
            #------- aceleracion de cada particula -----------------------------------------------------
            
            _a1 = q1 * (np.cross(_v1,_B) + q2*k*iR3*(dr)) / m1
            _a2 = q2 * (np.cross(_v2,_B) - q1*k*iR3*(dr)) / m2
            return np.concatenate((_v1, _v2, _a1, _a2))
            
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
        sol = solve_ivp(fun=ODE, t_span=[self.t0,self.tf], t_eval=self.time, y0 = self.ini_conditions)
        #-------------------------------------------------------------------------------------------
        self.r1, self.r2, self.v1, self.v2 = sol.y[0:3,:],sol.y[3:6,:],sol.y[6:9,:],sol.y[9:12,:]
        
        def all_acelerations(_r1,_r2,_v1,_v2):
            n = len(self.time)

            _v1,_v2 = np.transpose(_v1),np.transpose(_v2)
            _r1,_r2 = np.transpose(_r1),np.transpose(_r2)
            pcross1, pcross2, iR3 = np.zeros((n,3)), np.zeros((n,3)), np.zeros((n,3))

            dr = _r1-_r2

            #------- computing cross and dot product ---------------------------------------------------
            for i in range(n):
                pcross1[i,:] = np.cross(_v1[i,:],_B)
                pcross2[i,:] = np.cross(_v2[i,:],_B)
                iR3[i,:] = np.ones((1,3))*((1/np.dot(dr[i,:],dr[i,:]))**1.5)
            #------- aceleracion de cada particula -----------------------------------------------------
            _a1 = np.transpose( q1 * (pcross1 + q2*k*iR3*(dr)) / m1 )
            _a2 = np.transpose( q2 * (pcross2 - q1*k*iR3*(dr)) / m2 )
            return _a1,_a2
        #Acelerations
        self.a1, self.a2 = all_acelerations(self.r1,self.r2,self.v1,self.v2)
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Metodos para graficar
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def trajectory_plot(self, name, showing=True):
        '''
        This method plot the trajectory of the particules in
        a 3D plot
        '''

        fig = plt.figure(figsize=(8,8))
        axt = fig.add_subplot(1,1,1, projection='3d')
        axt.set_title('Gráfica de trayectoria', size=18)
        axt.plot(self.r1[0,:],self.r1[1,:],self.r1[2,:],label='Partícula 1',color='gold')
        axt.plot(self.r2[0,:],self.r2[1,:],self.r2[2,:],label='Partícula 2',color='indigo')
        axt.set_xlabel('x (m)')
        axt.set_ylabel('y (m)')
        axt.set_zlabel('z (m)')
        axt.legend()
        
        plt.savefig('PLOTS/'+name, dpi=250)
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def position_plot(self, name):
        '''
        This method plot de position of the particles in 3 2D-subplots (t,x), (t,y), (t,z)
        '''

        coords = [r'x $(m)$',r'y $(m)$',r'z $(m)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('''Gráfica de posición contra tiempo
        ''', size=19)
        for i in range(3):
            ax[i].plot(self.time,self.r1[i,:],label='Partícula 1',color='gold')
            ax[i].plot(self.time,self.r2[i,:],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (s)
        
        Condiciones iniciales: $r_1=${self.P1.r}, $r_2=${self.P2.r}, $v_1=${self.P1.v}, $v_2=${self.P2.v}, $B=${self.B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/'+name, dpi=250)
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def velocity_plot(self, name):
        '''
        This method plot de velocity of the particles in 3 2D-subplots (t,vx), (t,vy), (t,vz)
        '''
        coords = [r'vx $(m/s)$',r'vy $(m/s)$',r'vz $(m/s)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('''Gráfica de velocidad contra tiempo
        ''', size=19)
        for i in range(3):
            ax[i].plot(self.time,self.v1[i,:],label='Partícula 1',color='gold')
            ax[i].plot(self.time,self.v2[i,:],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (s)
        
        Condiciones iniciales: $r_1=${self.P1.r}, $r_2=${self.P2.r}, $v_1=${self.P1.v}, $v_2=${self.P2.v}, $B=${self.B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/'+name, dpi=250)
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def aceleration_plot(self, name):
        '''
        This method plot de aceleration of the particles in 3 2D-subplots (t,ax), (t,ay), (t,az)
        '''

        coords = [r'ax $(m/s^2)$',r'ay $(m/s^2)$',r'az $(m/s^2)$']
        fig,ax = plt.subplots(3,1,figsize=(9,8))
        ax[0].set_title('''Gráfica de aceleración contra tiempo
        ''', size=19)
        for i in range(3):
            ax[i].plot(self.time,self.a1[i,:],label='Partícula 1',color='gold')
            ax[i].plot(self.time,self.a2[i,:],label='Partícula 2',color='indigo')
            ax[i].set_ylabel(coords[i])
        ax[0].legend(bbox_to_anchor=(1.009, 1.3), loc='upper right')
        ax[0].set_xticklabels([])
        ax[1].set_xticklabels([])

        ax[2].set_xlabel(f'''Tiempo (s)
        
        Condiciones iniciales: $r_1=${self.P1.r}, $r_2=${self.P2.r}, $v_1=${self.P1.v}, $v_2=${self.P2.v}, $B=${self.B}''')
        fig.tight_layout()
        plt.savefig('PLOTS/'+name, dpi=250)