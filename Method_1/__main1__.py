from classes_1 import particle, interaction

particula1_1 = particle(1,10,[1,0,0],[-7.,-1.,1.])
particula2_1 = particle(1,5,[-2,0,0],[5.,-0.7,2.])

data_1 = interaction(particula1_1,particula2_1,[0,0,2],0,2,0.001)

data_1.solution()

data_1.trajectory_plot('example_1/3Dtrajectory_1.png')
data_1.position_plot('example_1/position(t)_1.png')
data_1.velocity_plot('example_1/velocity(t)_1.png')
data_1.aceleration_plot('example_1/acceleration(t)_1.png')

#--------------------------------------------------------------------

particula1_2 = particle(1,10,[0.5,0,0],[-4.,-0.5,1.])
particula2_2 = particle(1,-5,[-0.5,0,0],[5.,-0.7,1.])

data_2 = interaction(particula1_2,particula2_2,[0,0,2],0,2,0.001)

data_2.solution()

data_2.trajectory_plot('example_2/3Dtrajectory_2.png')
data_2.position_plot('example_2/position(t)_2.png')
data_2.velocity_plot('example_2/velocity(t)_2.png')
data_2.aceleration_plot('example_2/acceleration(t)_2.png')